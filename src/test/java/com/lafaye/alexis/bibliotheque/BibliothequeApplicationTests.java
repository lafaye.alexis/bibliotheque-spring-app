package com.lafaye.alexis.bibliotheque;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lafaye.alexis.bibliotheque.config.Config;
import com.lafaye.alexis.bibliotheque.dto.CreateBookDTO;
import com.lafaye.alexis.bibliotheque.dto.CreateUserDTO;
import com.lafaye.alexis.bibliotheque.dto.RegisterDTO;
import com.lafaye.alexis.bibliotheque.dto.UpdateEmpruntDTO;
import com.lafaye.alexis.bibliotheque.models.StructBook;
import com.lafaye.alexis.bibliotheque.models.StructUser;
import com.lafaye.alexis.bibliotheque.repositorises.StructBookRepository;
import com.lafaye.alexis.bibliotheque.repositorises.StructEmpruntRepository;
import com.lafaye.alexis.bibliotheque.repositorises.StructUserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.event.EventListener;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static org.hamcrest.Matchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
class BibliothequeApplicationTests {

	@Autowired
	private MockMvc mockMvc;
	@Autowired StructUserRepository userRepo;

	@Autowired StructBookRepository bookRepo;

	private final ObjectMapper mapper = new ObjectMapper();

	BibliothequeApplicationTests() {
	}

	private RequestPostProcessor adminAuth()
	{
		return httpBasic("admin", "password");
	}

	private RequestPostProcessor userAuth()
	{
		return httpBasic("user", "password");
	}

	@TestConfiguration
	static class DatabasePreparation {

		@Autowired StructUserRepository userRepo;
		@Autowired StructBookRepository bookRepo;
		@Autowired StructEmpruntRepository empruntRepo;

		@EventListener(ApplicationStartedEvent.class)
		public void preparedDbForTheTest()
		{
			// Réinitialiser la base de données de test avant de lancer les tests

			empruntRepo.deleteAll();
			userRepo.deleteAll();
			bookRepo.deleteAll();

			userRepo.save(new StructUser(null, "admin", "admin@admin.com", "ADMIN", new BCryptPasswordEncoder().encode("password"), 0));
			userRepo.save(new StructUser(null, "user", "user@user.com", "USER", new BCryptPasswordEncoder().encode("password"), 0));

			bookRepo.save(new StructBook(null, "Harry Potter à l'école des sorciers", "J. K. Rowling", Date.from(Instant.parse("1997-06-26T10:00:00.00Z")), 2));
			bookRepo.save(new StructBook(null, "Harry Potter et la chambre des secrets", "J. K. Rowling", Date.from(Instant.parse("1998-07-02T10:00:00.00Z")), 2));
			bookRepo.save(new StructBook(null, "Harry Potter  et le Prisonnier d'Azkaban", "J. K. Rowling", Date.from(Instant.parse("1999-07-08T10:00:00.00Z")), 2));
			bookRepo.save(new StructBook(null, "Harry Potter et la coupe de feu", "J. K. Rowling", Date.from(Instant.parse("2000-07-08T10:00:00.00Z")), 2));
		}

	}

	@Test
	void contextLoads()
	{
	}

	@Test
	void loginShouldBeAccessible() throws Exception
	{
		mockMvc.perform(get("/login"))
			   .andExpect(status().isOk());
	}

	@Test
	void testAuth() throws Exception
	{
		// Vérifier que l'authentification est nécessaire

		mockMvc.perform(get("/users/me"))
				.andExpect(status().isUnauthorized());

		// Vérifier l'authentification

		mockMvc.perform(get("/users/me").with(adminAuth()))
				.andExpect(status().isOk())
				.andExpect(jsonPath("username", is("admin")));

		mockMvc.perform(get("/users/me").with(userAuth()))
				.andExpect(status().isOk())
				.andExpect(jsonPath("username", is("user")));

		// Vérifier les accés des rôles USER et ADMIN

		mockMvc.perform(get("/users").with(adminAuth()))
				.andExpect(status().isOk());

		mockMvc.perform(get("/users").with(userAuth()))
				.andExpect(status().isForbidden());
	}

	@Test
	void testUsers() throws Exception
	{
		// Test de création d'utilisateur

		CreateUserDTO createDTO = new CreateUserDTO("toto", "toto@toto.com", "password", "USER");

		MvcResult result = mockMvc.perform(post("/users").with(adminAuth()).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(createDTO)))
				.andExpect(status().isCreated())
				.andReturn();

		final Long userId = mapper.readValue(result.getResponse().getContentAsString(), StructUser.class).getUserId();

		// Vérifier qu'on a maintenant 3 utilisateurs 'admin', 'user' et 'toto'

		mockMvc.perform(get("/users").with(adminAuth()))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(3)))
				.andExpect(jsonPath("$[0].username", is("admin")))
				.andExpect(jsonPath("$[1].username", is("user")))
				.andExpect(jsonPath("$[2].username", is("toto")));

		// Test de la mise à jour d'utilisateur

		RegisterDTO updateDTO = new RegisterDTO(null, "toto2@toto.com", null);

		mockMvc.perform(post("/users/" + userId).with(adminAuth()).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(updateDTO)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("email", is("toto2@toto.com")));

		// Test de la suppression d'utilisateur

		mockMvc.perform(delete("/users/" + userId).with(adminAuth()))
				.andExpect(status().isOk());

		// Vérifier qu'on n'a maintenant plus qu'un utilisateur 'admin'

		mockMvc.perform(get("/users").with(adminAuth()))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)))
				.andExpect(jsonPath("$[0].username", is("admin")))
				.andExpect(jsonPath("$[1].username", is("user")));
	}

	@Test
	void testBooks() throws Exception
	{
		// Test de la création d'un livre

		CreateBookDTO createDTO = new CreateBookDTO("Les Misérables", "Gustave Flaubert", Date.from(Instant.parse("1862-01-01T10:15:30.00Z")), 3);

		MvcResult result = mockMvc.perform(post("/books").with(adminAuth()).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(createDTO)))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("title", is(createDTO.getTitle())))
				.andReturn();

		Long bookId = mapper.readValue(result.getResponse().getContentAsString(), StructBook.class).getBookId();

		// Test de la modification d'un livre

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		df.setTimeZone(TimeZone.getTimeZone("UTC"));

		CreateBookDTO updateDTO = new CreateBookDTO(null, "Victor Hugo", null, 5);

		mockMvc.perform(post("/books/" + bookId).with(adminAuth()).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(updateDTO)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("title", is(createDTO.getTitle())))
				.andExpect(jsonPath("author", is(updateDTO.getAuthor())))
				.andExpect(jsonPath("releaseDate", is(df.format(createDTO.getReleaseDate()).replace("Z", "+00:00"))))
				.andExpect(jsonPath("numAvailable", is(updateDTO.getNumAvailable())));

		// Test de la recherche

		// Recherche par auteur
		mockMvc.perform(get("/books/search").with(userAuth()).param("author", "hugo"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].author", is(updateDTO.getAuthor())))
				.andExpect(jsonPath("$[0].title", is(createDTO.getTitle())));

		// Recherche par titre
		mockMvc.perform(get("/books/search").with(userAuth()).param("title", "misérables"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].author", is(updateDTO.getAuthor())))
				.andExpect(jsonPath("$[0].title", is(createDTO.getTitle())));

		// Test de la suppression de livre

		mockMvc.perform(delete("/books/" + bookId).with(adminAuth()))
				.andExpect(status().isOk());

		mockMvc.perform(get("/books").with(userAuth()))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(4)));
	}

	@Test
	void testEmprunts() throws Exception
	{
		// On récupère d'abord les IDs des livres

		List<String> bookIdsStrings = bookRepo.findAll().stream().map(book -> book.getBookId().toString()).toList();

		// Test de la création d'un emprunt et de la limite d'emprunts (3 max)

		for (int i = 0; i <= Config.MAX_ACTIVE_EMPRUNTS_PER_USER; ++i)
		{
			mockMvc.perform(post("/emprunts").with(userAuth()).param("bookId", bookIdsStrings.get(i)))
					.andExpect(i < Config.MAX_ACTIVE_EMPRUNTS_PER_USER ? status().isCreated() : status().isForbidden());
		}

		// Tester le retour d'un emprunt

		mockMvc.perform(delete("/emprunts").with(userAuth()).param("bookId", bookIdsStrings.get(0)))
				.andExpect(status().isOk());

		mockMvc.perform(get("/emprunts").with(userAuth()))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].returnDate", notNullValue()));

		// Tester la mise à jour d'un emprunt par l'admin

		// On rajoute 20 jours à la date de retour de l'emprunt

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.DAY_OF_MONTH, 20);

		Long userId = userRepo.findByUsername("user").getUserId();

		UpdateEmpruntDTO updateDTO = new UpdateEmpruntDTO(userId, Long.parseLong(bookIdsStrings.get(0)), calendar.getTime(), new Date());

		mockMvc.perform(post("/emprunts/edit").with(adminAuth()).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(updateDTO)))
				.andExpect(status().isOk());

		// Test de la comptabilisation des retards

		mockMvc.perform(get("/emprunts/late").with(adminAuth()).param("userId", userId.toString()))
				.andExpect(status().isOk())
				.andExpect(jsonPath("numLateEmprunts", is(1)));
	}
}
