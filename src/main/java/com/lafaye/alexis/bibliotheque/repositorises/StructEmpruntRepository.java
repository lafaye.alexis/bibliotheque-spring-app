package com.lafaye.alexis.bibliotheque.repositorises;

import com.lafaye.alexis.bibliotheque.models.StructEmprunt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StructEmpruntRepository extends CustomRepository<StructEmprunt, StructEmprunt.EmpruntID> {

    List<StructEmprunt> findAllByEmpruntIdUserId(Long userId);
    List<StructEmprunt> findAllByEmpruntIdBookId(Long bookId);

}
