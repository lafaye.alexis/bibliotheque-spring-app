package com.lafaye.alexis.bibliotheque.repositorises;

import com.lafaye.alexis.bibliotheque.models.StructUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StructUserRepository extends CustomRepository<StructUser, Long> {

    StructUser findByUsernameOrEmail(String username, String email);

    StructUser findByUsername(String username);
}
