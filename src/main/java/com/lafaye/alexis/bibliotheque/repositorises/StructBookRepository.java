package com.lafaye.alexis.bibliotheque.repositorises;

import com.lafaye.alexis.bibliotheque.models.StructBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StructBookRepository extends CustomRepository<StructBook, Long> {

    List<StructBook> findByAuthorContainingIgnoreCase(String author);

    List<StructBook> findByTitleContainingIgnoreCase(String title);

    List<StructBook> findByTitleContainingIgnoreCaseAndAuthorContainingIgnoreCase(String title, String author);

}
