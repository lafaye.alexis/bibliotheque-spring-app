package com.lafaye.alexis.bibliotheque.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CreateUserDTO {

    private String username;
    private String email;
    private String password;
    private String role;

}
