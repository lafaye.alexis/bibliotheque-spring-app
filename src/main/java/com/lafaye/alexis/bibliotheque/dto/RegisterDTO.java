package com.lafaye.alexis.bibliotheque.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class RegisterDTO {

    private String username;
    private String email;
    private String password;

}
