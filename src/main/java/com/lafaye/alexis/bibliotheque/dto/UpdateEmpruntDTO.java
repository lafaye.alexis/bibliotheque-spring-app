package com.lafaye.alexis.bibliotheque.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@Getter
@AllArgsConstructor
public class UpdateEmpruntDTO {

    private Long userId;
    private Long bookId;
    private Date returnDate;
    private Date limitReturnDate;

}
