package com.lafaye.alexis.bibliotheque.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
public class LateEmpruntsDTO {

    private int numLateEmprunts;

}
