package com.lafaye.alexis.bibliotheque.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@Getter
@AllArgsConstructor
public class CreateBookDTO {

    private String title;
    private String author;
    private Date releaseDate;
    private Integer numAvailable;

}
