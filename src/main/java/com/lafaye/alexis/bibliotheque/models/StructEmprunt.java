package com.lafaye.alexis.bibliotheque.models;

import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "emprunt")
@Entity
public class StructEmprunt implements Serializable {

    // On fait dériver cette classe Summary des interfaces Summary de StructUser et de StructBook
    // afin que la serialisation prenne en compte les annotations @JsonView de StructUser et de StructBook
    public static class Summary implements StructUser.Summary, StructBook.Summary {}

    @Embeddable
    @NoArgsConstructor
    @AllArgsConstructor
    @EqualsAndHashCode
    @Data
    public static class EmpruntID implements Serializable
    {
        @Column(name = "user_id")
        private Long userId;

        @Column(name = "book_id")
        private Long bookId;
    }

    @EmbeddedId
    @Hidden
    private EmpruntID empruntId = new EmpruntID();

    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    StructUser user;

    @ManyToOne
    @MapsId("bookId")
    @JoinColumn(name = "book_id")
    @JsonView(Summary.class)
    StructBook book;

    @JsonView(Summary.class)
    Date returnDate;

    @JsonView(Summary.class)
    Date limitReturnDate;

    // On utilise ce getter afin qu'à la serialisation, l'attribut "user" ne contienne que les attributs
    // annotés par @JsonView
    @JsonView(Summary.class)
    @ManyToOne
    @MapsId("userId")
    public StructUser getUser()
    {
        return user;
    }

}
