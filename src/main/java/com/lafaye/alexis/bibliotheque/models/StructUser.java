package com.lafaye.alexis.bibliotheque.models;

import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users")
@Entity
public class StructUser {

    public enum Role
    {
        USER,
        ADMIN
    }

    public interface Summary {}

    @Hidden
    public boolean isValid()
    {
        return username != null
                && email != null
                && password != null
                && role != null;
    }

    @Id
    @GeneratedValue
    @JsonView(Summary.class)
    private Long userId;

    @Column(unique = true)
    @JsonView(Summary.class)
    private String username;

    @Column(unique = true)
    @JsonView(Summary.class)
    private String email;

    @JsonView(Summary.class)
    private String role;

    @Hidden
    private String password;

    @Hidden
    private int numLateEmprunts;
}
