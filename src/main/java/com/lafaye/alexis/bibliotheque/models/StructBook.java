package com.lafaye.alexis.bibliotheque.models;

import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "book")
@Entity
public class StructBook {

    public interface Summary {}

    @Hidden
    public boolean isValid()
    {
        return !title.isEmpty()
                && !author.isEmpty()
                && releaseDate != null
                && numAvailable != null
                && numAvailable >= 0;
    }

    @Id
    @GeneratedValue
    @JsonView(Summary.class)
    private Long bookId;

    @JsonView(Summary.class)
    private String title;

    @JsonView(Summary.class)
    private String author;

    @JsonView(Summary.class)
    private Date releaseDate;

    @JsonView(Summary.class)
    private Integer numAvailable;
}
