package com.lafaye.alexis.bibliotheque.services;

import com.lafaye.alexis.bibliotheque.dto.UpdateEmpruntDTO;
import com.lafaye.alexis.bibliotheque.models.StructEmprunt;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;

import java.util.List;

public interface EmpruntService {

    class BookUnavailableException extends Exception { }
    class MaxActiveEmpruntsReachedException extends Exception { }

    /**
     * Obtenir un emprunt
     * @param userId L'ID de l'utilisateur
     * @param bookId L'ID du livre
     * @return L'emprunt correspondant, ou null si inexistant
     */
    StructEmprunt getEmprunt(Long userId, Long bookId);

    /**
     * Obtenir tous les emprunts réalisés par tous les utilisateurs
     * @return Liste des emprunts
     */
    List<StructEmprunt> getEmprunts();

    /**
     * Obtenir tous les emprunts réalisés par tous les utilisateurs
     * @param active Si true, retourne les emprunts actifs (c-à-d pas encore retournés), sinon retourne les emprunts déjà rendus
     * @return Liste des emprunts
     */
    List<StructEmprunt> getEmprunts(boolean active);

    /**
     * Obtenir tous les emprunts réalisés par un utilisateur
     * @param userId L'ID de l'utilisateur
     * @return La liste des emprunts de l'utilisateur
     */
    List<StructEmprunt> getUserEmprunts(Long userId);

    /**
     * Obtenir tous les emprunts réalisés par un utilisateur
     * @param userId L'ID de l'utilisateur
     * @param active Si true, retourne les emprunts actifs (c-à-d pas encore retournés), sinon retourne les emprunts déjà rendus
     * @return La liste des emprunts de l'utilisateur
     */
    List<StructEmprunt> getUserEmprunts(Long userId, boolean active);

    /**
     * Emprunter un livre pour un utilisateur
     * @param userId L'ID de l'utilisateur
     * @param bookId L'ID du livre à emprunter
     * @return Le nouvel emprunt créé
     * @throws EntityNotFoundException Si l'utilisateur et/ou le livre n'a pas été trouvé
     * @throws EntityExistsException Si l'utilisateur a déjà un emprunt actif pour ce livre
     * @throws BookUnavailableException Si le livre demandé n'est pas disponible (numAvailable == 0)
     * @throws MaxActiveEmpruntsReachedException Si l'utilisateur a déjà atteint la limite d'emprunts actifs simultanés possible
     */
    StructEmprunt addEmprunt(Long userId, Long bookId) throws EntityNotFoundException, EntityExistsException, BookUnavailableException, MaxActiveEmpruntsReachedException;

    /**
     * Retourner un emprunt pour un utilisateur
     * @param userId L'ID de l'utilisateur
     * @param bookId L'ID du livre à retourner
     * @return L'emprunt mis à jour avec la date de retour
     * @throws EntityNotFoundException Si l'emprunt n'a pas été trouvé
     * @throws EntityExistsException Si l'emprunt spécifié a déjà été retourné
     */
    StructEmprunt returnEmprunt(Long userId, Long bookId) throws EntityNotFoundException, EntityExistsException;

    /**
     * Mettre à jour un emrpunt
     * @param inEmprunt L'emprunt modifié. Si returnDate est null, l'emprunt sera marqué comme actif
     * @return L'emprunt modifié
     * @throws EntityNotFoundException Si l'emprunt n'a pas été trouvé
     * @throws EntityExistsException Si l'emprunt modifié est identique à l'emprunt actuel
     */
    StructEmprunt updateEmprunt(UpdateEmpruntDTO inEmprunt) throws EntityNotFoundException, EntityExistsException;

    /**
     * Obtenir le nombre d'emprunts rendus en retard par un utilisateur
     * @param userId L'ID de l'utilisateur
     * @return Le nombre total d'emprunts rendus en retard
     * @throws EntityNotFoundException Si l'utilisateur n'existe pas
     */
    int getNumLateEmprunts(Long userId) throws EntityNotFoundException;
}
