package com.lafaye.alexis.bibliotheque.services;

import com.lafaye.alexis.bibliotheque.dto.CreateBookDTO;
import com.lafaye.alexis.bibliotheque.dto.CreateUserDTO;
import com.lafaye.alexis.bibliotheque.models.StructUser;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.List;

public interface UserService {

    /**
     * Obtenir la liste de tous les utilisateurs
     * @return Liste des utilisateurs
     */
    List<StructUser> getAllUsers();

    /**
     * Obtenir un utilisateur par son ID
     * @param userId L'ID de l'utilisateur
     * @return L'utilisateur, ou null si inexistant
     */
    StructUser getUserById(Long userId);

    /**
     * Obtenir un utilisateur par son nom
     * @param username Le nom d'utilisateur
     * @return L'utilisateur, ou null si inexistant
     */
    StructUser getUserByUsername(String username);

    /**
     * Ajouter un nouvel utilisateur
     * @param inUser L'utilisateur à créer
     * @return L'utilisateur nouvellement créé
     * @throws IllegalArgumentException Si un ou plusieurs champs sont invalides
     * @throws DataIntegrityViolationException Si l'utilisateur ajouté entre en conflit avec un autre utilisateur
     */
    StructUser createUser(CreateUserDTO inUser) throws IllegalArgumentException, DataIntegrityViolationException;

    /**
     * Mettre à jour les infos d'un utilisateur
     * @param userId L'ID de l'utilisateur à modifier
     * @param inUser Les nouvelles infos de l'utilisateur. Seuls les paramètres non-nuls seront utilisés
     * @return L'utilisateur modifés
     * @throws EntityNotFoundException Si l'utilisateur n'existe pas
     * @throws EntityExistsException Si les nouvelels infos sont identiques à celles de l'utilisateur actuel
     * @throws DataIntegrityViolationException Si l'utilisateur modifié entre en conflit avec un autre utilisateur
     */
    StructUser updateUser(Long userId, CreateUserDTO inUser) throws EntityNotFoundException, EntityExistsException, DataIntegrityViolationException;

    /**
     * Ajouter un retour en retard à l'utilisateur
     * @param userId L'ID de l'utilisateur
     * @return L'utilisateur modifié
     * @throws EntityNotFoundException Si l'utilisateur n'esxiste pas
     */
    StructUser addLateReturn(Long userId) throws EntityNotFoundException;

    /**
     * Retirer un retour en retard à l'utilisateur
     * @param userId L'ID de l'utilisateur
     * @return L'utilisateur modifié
     */
    StructUser removeLateReturn(Long userId) throws EntityNotFoundException;

    /**
     * Supprimer un utilisateur
     * @param userId L'ID de l'utilisateur à supprimer
     * @throws EntityNotFoundException Si l'utilisateur n'esxiste pas
     */
    void deleteUser(Long userId) throws EntityNotFoundException;
}
