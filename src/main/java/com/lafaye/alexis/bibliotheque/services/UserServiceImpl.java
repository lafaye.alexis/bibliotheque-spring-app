package com.lafaye.alexis.bibliotheque.services;

import com.lafaye.alexis.bibliotheque.dto.CreateBookDTO;
import com.lafaye.alexis.bibliotheque.dto.CreateUserDTO;
import com.lafaye.alexis.bibliotheque.models.StructUser;
import com.lafaye.alexis.bibliotheque.repositorises.StructUserRepository;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final StructUserRepository userRepo;

    private final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    public UserServiceImpl(StructUserRepository inUserRepo)
    {
        userRepo = inUserRepo;

        // Si la base ne contient pas d'user, on crée l'admin

        if (getAllUsers().size() == 0)
        {
            createUser(new CreateUserDTO("admin", "admin@admin.com", "password", "ADMIN"));
        }
    }

    @Override
    public List<StructUser> getAllUsers() {
        return userRepo.findAll();
    }

    @Override
    public StructUser getUserById(Long userId) {
        Optional<StructUser> user = userRepo.findById(userId);

        return user.isEmpty() ? null : user.get();
    }

    @Override
    public StructUser getUserByUsername(String username) {
        return userRepo.findByUsername(username);
    }

    @Override
    public StructUser createUser(CreateUserDTO inUser) throws IllegalArgumentException, DataIntegrityViolationException
    {
        StructUser user = new StructUser(null, inUser.getUsername(), inUser.getEmail(), inUser.getRole(), encoder.encode(inUser.getPassword()), 0);

        // On vérifie la validité des arguments passés avec la méthode isValid() de StructUser
        // Si un ou plusieurs arguments sont null, on retourne un code "Bad Request"

        if (!user.isValid())
        {
            throw new IllegalArgumentException();
        }

        // On vérifie que le role passé existe bien dans l'enum Role
        // Cette expression n'a pas d'effet mais sert à throw un IllegalArgumentException si le role n'existe pas dans l'enum
        StructUser.Role.valueOf(user.getRole());

        return userRepo.save(user);
    }

    @Override
    public StructUser updateUser(Long userId, CreateUserDTO inUser)
            throws EntityNotFoundException, EntityExistsException, DataIntegrityViolationException
    {
        final String username = inUser.getUsername();
        final String email = inUser.getEmail();
        final String password = inUser.getPassword();
        final String role = inUser.getRole();

        Optional<StructUser> userOpt = userRepo.findById(userId);

        if (userOpt.isEmpty())
        {
            throw new EntityNotFoundException();
        }

        // On modifie seulement les informations passées dans le body, on ignore celles qui sont null

        StructUser user = userOpt.get();
        boolean modified = false;

        if (username != null && !username.isEmpty() && !username.equals(user.getUsername()))
        {
            user.setUsername(username);
            modified = true;
        }

        if (email != null && !email.isEmpty() && !email.equals(user.getEmail()))
        {
            user.setEmail(email);
            modified = true;
        }

        if (password != null && !password.isEmpty())
        {
            user.setPassword(encoder.encode(password));
            modified = true;
        }

        if (role != null && !user.getRole().equals(role))
        {
            StructUser.Role.valueOf(role); // Cette expression n'a aucun effet, mais permet de throw une IllegalArgumentException si le rôle est invalide

            user.setRole(role);
            modified = true;
        }

        if (!modified)
        {
            // Aucun champ n'a été modifié
            throw new EntityExistsException();
        }

        user = userRepo.save(user);

        return user;
    }

    @Override
    public StructUser addLateReturn(Long userId) throws EntityNotFoundException
    {
        StructUser user = getUserById(userId);

        if (user == null)
        {
            throw new EntityNotFoundException();
        }

        user.setNumLateEmprunts(user.getNumLateEmprunts() + 1);
        return userRepo.save(user);
    }

    @Override
    public StructUser removeLateReturn(Long userId) throws EntityNotFoundException
    {
        StructUser user = getUserById(userId);

        if (user == null)
        {
            throw new EntityNotFoundException();
        }

        user.setNumLateEmprunts(user.getNumLateEmprunts() - 1);
        return userRepo.save(user);
    }

    @Override
    public void deleteUser(Long userId)
    {
        if (userRepo.findById(userId).isEmpty())
        {
            throw new EntityNotFoundException();
        }

        userRepo.deleteById(userId);
    }
}
