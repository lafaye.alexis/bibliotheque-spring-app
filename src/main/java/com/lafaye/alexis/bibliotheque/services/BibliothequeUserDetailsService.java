package com.lafaye.alexis.bibliotheque.services;

import com.lafaye.alexis.bibliotheque.models.StructUser;
import com.lafaye.alexis.bibliotheque.repositorises.StructUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class BibliothequeUserDetailsService implements UserDetailsService {
    @Autowired
    StructUserRepository userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        StructUser user = userRepo.findByUsernameOrEmail(username, username);

        if(user==null)
        {
            throw new UsernameNotFoundException("User does not exist by Username");
        }

        return new User(user.getUsername(), user.getPassword(), buildUserAuthority(user.getRole()));
    }

    private List<GrantedAuthority> buildUserAuthority(String role) {
        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
        setAuths.add(new SimpleGrantedAuthority(role));

        return new ArrayList<GrantedAuthority>(setAuths);
    }
}
