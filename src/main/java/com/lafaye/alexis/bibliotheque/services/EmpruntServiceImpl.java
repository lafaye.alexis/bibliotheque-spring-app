package com.lafaye.alexis.bibliotheque.services;

import com.lafaye.alexis.bibliotheque.config.Config;
import com.lafaye.alexis.bibliotheque.dto.CreateBookDTO;
import com.lafaye.alexis.bibliotheque.dto.UpdateEmpruntDTO;
import com.lafaye.alexis.bibliotheque.models.StructBook;
import com.lafaye.alexis.bibliotheque.models.StructEmprunt;
import com.lafaye.alexis.bibliotheque.models.StructUser;
import com.lafaye.alexis.bibliotheque.repositorises.StructEmpruntRepository;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.*;

@Service
public class EmpruntServiceImpl implements EmpruntService {
    private final StructEmpruntRepository empruntRepo;
    private final BookServiceImpl bookService;
    private final UserServiceImpl userService;

    public EmpruntServiceImpl(StructEmpruntRepository inEmpruntRepo, BookServiceImpl inBookService, UserServiceImpl inUserService)
    {
        empruntRepo = inEmpruntRepo;
        bookService = inBookService;
        userService = inUserService;
    }

    @Override
    public StructEmprunt getEmprunt(Long userId, Long bookId)
    {
        Optional<StructEmprunt> emprunt = empruntRepo.findById(new StructEmprunt.EmpruntID(userId, bookId));

        return emprunt.isEmpty() ? null : emprunt.get();
    }

    @Override
    public List<StructEmprunt> getEmprunts()
    {
        return empruntRepo.findAll();
    }

    @Override
    public List<StructEmprunt> getEmprunts(boolean active)
    {
        return empruntRepo.findAll().stream().filter(emprunt -> active == (emprunt.getReturnDate() == null)).toList();
    }

    @Override
    public List<StructEmprunt> getUserEmprunts(Long userId)
    {
        return empruntRepo.findAllByEmpruntIdUserId(userId);
    }

    @Override
    public List<StructEmprunt> getUserEmprunts(Long userId, boolean active)
    {
        List<StructEmprunt> emprunts = getUserEmprunts(userId);

        return emprunts.stream().filter(emprunt -> active == (emprunt.getReturnDate() == null)).toList();
    }

    @Override
    public StructEmprunt addEmprunt(Long userId, Long bookId)
            throws EntityNotFoundException, EntityExistsException, EmpruntService.BookUnavailableException, MaxActiveEmpruntsReachedException
    {
        StructBook book = bookService.getBook(bookId);
        StructUser user = userService.getUserById(userId);

        if (book == null || user == null)
        {
            throw new EntityNotFoundException();
        }

        // On bloque l'emprunt si le livre n'est pas disponible

        if (book.getNumAvailable().equals(0))
        {
            throw new BookUnavailableException();
        }

        // On bloque l'emprunt s l'utilisateur a déjà le nombre maximum d'emprunts actifs autorisés

        if (getUserEmprunts(userId, true).size() >= Config.MAX_ACTIVE_EMPRUNTS_PER_USER)
        {
            throw new MaxActiveEmpruntsReachedException();
        }

        StructEmprunt emprunt = getEmprunt(userId, bookId);
        Date limitReturnDate = new Date(new Date().getTime() + Config.EMPRUNT_DURATION.toMillis());

        if (emprunt != null)
        {
            // On empêche l'emprunt d'un livre s'il est déjà en cours d'emprunt par l'utilisateur
            if (emprunt.getReturnDate() == null)
            {
                throw new EntityExistsException();
            }

            // Si le livre a déjà été retourné, l'utilisateur peut l'emprunter à nouveau
            // On réinitialise alors la date de retour pour marquer cet emprunt comme actif
            emprunt.setReturnDate(null);
            emprunt.setLimitReturnDate(limitReturnDate);
        }
        else
        {
            emprunt = new StructEmprunt(new StructEmprunt.EmpruntID(userId, bookId),
                                        user,
                                        book,
                                        null,
                                        limitReturnDate);
        }

        emprunt = empruntRepo.save(emprunt);

        bookService.updateBook(bookId, new CreateBookDTO(null, null, null, book.getNumAvailable() - 1));

        return emprunt;
    }

    @Override
    public StructEmprunt returnEmprunt(Long userId, Long bookId) throws EntityNotFoundException, EntityExistsException
    {
        StructBook book = bookService.getBook(bookId);
        StructUser user = userService.getUserById(userId);
        StructEmprunt emprunt = getEmprunt(userId, bookId);

        if (book == null || user == null || emprunt == null)
        {
            throw new EntityNotFoundException();
        }

        if (emprunt.getReturnDate() != null)
        {
            throw new EntityExistsException();
        }

        emprunt.setReturnDate(new Date());

        if (Duration.between(emprunt.getReturnDate().toInstant(), emprunt.getLimitReturnDate().toInstant()).isNegative())
        {
            // Si la date de retour est ultérieure à la date limite, on rajoute un retard à l'user
            userService.addLateReturn(user.getUserId());
        }

        // On rajoute l'exmplaire du livre rendu comme disponible
        bookService.updateBook(bookId, new CreateBookDTO(null, null, null, book.getNumAvailable() + 1));

        return empruntRepo.save(emprunt);
    }

    @Override
    public StructEmprunt updateEmprunt(UpdateEmpruntDTO inEmprunt) throws EntityNotFoundException, EntityExistsException
    {
        StructEmprunt emprunt = getEmprunt(inEmprunt.getUserId(), inEmprunt.getBookId());

        if (emprunt == null)
        {
            throw new EntityNotFoundException();
        }

        boolean modified = false;
        boolean wasLate = Duration.between(emprunt.getReturnDate().toInstant(), emprunt.getLimitReturnDate().toInstant()).isNegative();

        if (!Objects.equals(inEmprunt.getReturnDate(), emprunt.getReturnDate()))
        {
            emprunt.setReturnDate(inEmprunt.getReturnDate());
            modified = true;
        }

        if (!Objects.equals(inEmprunt.getLimitReturnDate(), emprunt.getLimitReturnDate()))
        {
            emprunt.setLimitReturnDate(inEmprunt.getLimitReturnDate());
            modified = true;
        }

        if (!modified)
        {
            throw new EntityExistsException();
        }

        boolean isLate = Duration.between(emprunt.getReturnDate().toInstant(), emprunt.getLimitReturnDate().toInstant()).isNegative();

        // On met à jour le nombre de retards de l'utilisateur en fonction des nouvelles dates de cet emprunt

        if (wasLate && !isLate)
        {
            userService.removeLateReturn(inEmprunt.getUserId());
        }
        else if (!wasLate && isLate)
        {
            userService.addLateReturn(inEmprunt.getUserId());
        }

        return empruntRepo.save(emprunt);
    }

    @Override
    public int getNumLateEmprunts(Long userId) throws EntityNotFoundException
    {
        StructUser user = userService.getUserById(userId);

        if (user == null)
        {
            throw new EntityNotFoundException();
        }

        List<StructEmprunt> emprunts = getUserEmprunts(user.getUserId());
        int numLate = user.getNumLateEmprunts();

        // On ajoute au nombre de retards les emprunts pas encore rendus dont la date limite est déjà dépassée

        for (StructEmprunt emprunt : emprunts)
        {
            if (emprunt.getReturnDate() != null)
            {
                continue;
            }

            if (Duration.between(new Date().toInstant(), emprunt.getLimitReturnDate().toInstant()).isNegative())
            {
                // Si la date de retour est ultérieure à la date limite, on rajoute un retard à l'user
                numLate++;
            }
        }

        return numLate;
    }
}
