package com.lafaye.alexis.bibliotheque.services;

import com.lafaye.alexis.bibliotheque.dto.CreateBookDTO;
import com.lafaye.alexis.bibliotheque.models.StructBook;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;

import java.util.Date;
import java.util.List;

public interface BookService {

    /**
     * Obtenir tous les livres
     * @return Liste de tous les livres
     */
    List<StructBook> getAllBooks();

    /**
     * Obtenir un livre par son ID
     * @param bookId ID du livre à obtenir
     * @return Le livre obtenu, ou null si inexistant
     */
    StructBook getBook(Long bookId);

    /**
     * Ajouter un nouveau livre
     * @param inBook Les infos du nouveau livre
     * @return Le livre nouvellement créé
     * @throws IllegalArgumentException Si les infos passées ne sont pas valides
     */
    StructBook createBook(CreateBookDTO inBook) throws IllegalArgumentException;

    /**
     * Supprimer un livre
     * @param bookId L'ID du livre à supprimer
     * @throws EntityNotFoundException Si le livre spécifié n'existe pas
     */
    void deleteBook(Long bookId) throws EntityNotFoundException;

    /**
     * Mettre à jour les infos d'un livre existant
     * @param bookId L'ID du livre à modifier
     * @param inBook Les nouvelles infos. Seuls les champs non-nuls seront utilisés
     * @return Le livre modifié
     * @throws EntityNotFoundException Si le livre spécifié n'existe pas
     * @throws EntityExistsException Si les nouvelles infos sont identiques au livre existant
     */
    StructBook updateBook(Long bookId, CreateBookDTO inBook) throws EntityNotFoundException, EntityExistsException;

    /**
     * Rechercher un livre par titre et/ou auteur (insensible à la casse)
     * @param title Requête pour le titre
     * @param author Requête pour l'auteur
     * @return La liste des livres correspondant à la recherche
     * @throws IllegalArgumentException Si les 2 arguments sont nuls ou vides
     */
    List<StructBook> searchBooks(String title, String author) throws IllegalArgumentException;

}
