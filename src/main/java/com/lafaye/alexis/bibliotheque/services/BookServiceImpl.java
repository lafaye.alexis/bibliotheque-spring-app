package com.lafaye.alexis.bibliotheque.services;

import com.lafaye.alexis.bibliotheque.dto.CreateBookDTO;
import com.lafaye.alexis.bibliotheque.models.StructBook;
import com.lafaye.alexis.bibliotheque.repositorises.StructBookRepository;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {

    private final StructBookRepository bookRepo;

    public BookServiceImpl(StructBookRepository inBookRepo)
    {
        bookRepo = inBookRepo;
    }

    @Override
    public List<StructBook> getAllBooks() {
        return bookRepo.findAll();
    }

    @Override
    public StructBook getBook(Long bookId) {
        Optional<StructBook> book = bookRepo.findById(bookId);

        return book.isEmpty() ? null : book.get();
    }

    @Override
    public StructBook createBook(CreateBookDTO inBook) throws IllegalArgumentException
    {
        // On laisse l'ID à null pour qu'il soit auto-généré
        StructBook book = new StructBook(null,
                                         inBook.getTitle(),
                                         inBook.getAuthor(),
                                         inBook.getReleaseDate(),
                                         inBook.getNumAvailable() != null ? inBook.getNumAvailable() : 0);

        if (!book.isValid())
        {
            throw new IllegalArgumentException();
        }

        return bookRepo.save(book);
    }

    @Override
    public void deleteBook(Long bookId) throws EntityNotFoundException
    {
        StructBook book = getBook(bookId);

        if (book == null)
        {
            throw new EntityNotFoundException();
        }

        bookRepo.delete(book);
    }

    @Override
    public StructBook updateBook(Long bookId, CreateBookDTO inBook)
            throws EntityNotFoundException, EntityExistsException
    {
        StructBook book = getBook(bookId);

        if (book == null)
        {
            throw new EntityNotFoundException();
        }

        String title = inBook.getTitle();
        String author = inBook.getAuthor();
        Date releaseDate = inBook.getReleaseDate();
        Integer numAvailable = inBook.getNumAvailable();

        // On ne va mettre à jour que les paramètres non-nuls
        boolean modified = false;

        if (title != null && !title.isEmpty() && !title.equals(book.getTitle()))
        {
            book.setTitle(title);
            modified = true;
        }

        if (author != null && !author.isEmpty() && !author.equals(book.getAuthor()))
        {
            book.setAuthor(author);
            modified = true;
        }

        if (releaseDate != null && !releaseDate.equals(book.getReleaseDate()))
        {
            book.setReleaseDate(releaseDate);
            modified = true;
        }

        if (numAvailable != null && numAvailable >= 0 && !numAvailable.equals(book.getNumAvailable()))
        {
            book.setNumAvailable(numAvailable);
            modified = true;
        }

        if (!modified)
        {
            // Aucun paramètre n'a été modifié

            throw new EntityExistsException();
        }

        return bookRepo.save(book);
    }

    @Override
    public List<StructBook> searchBooks(String title, String author) throws IllegalArgumentException
    {
        if (title != null && author != null && !title.isEmpty() && !author.isEmpty())
        {
            return bookRepo.findByTitleContainingIgnoreCaseAndAuthorContainingIgnoreCase(title, author);
        }

        if (title != null && !title.isEmpty())
        {
            return bookRepo.findByTitleContainingIgnoreCase(title);
        }

        if (author != null && !author.isEmpty())
        {
            return bookRepo.findByAuthorContainingIgnoreCase(author);
        }

        // Aucun paramètre n'était correct

        throw new IllegalArgumentException();
    }
}
