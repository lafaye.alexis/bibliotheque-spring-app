package com.lafaye.alexis.bibliotheque.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.lafaye.alexis.bibliotheque.dto.CreateBookDTO;
import com.lafaye.alexis.bibliotheque.models.StructBook;
import com.lafaye.alexis.bibliotheque.services.BookServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/books") // Toutes les requêtes de ce controller seront sous la route /books
public class BookController {

    private final BookServiceImpl bookService;

    public BookController(BookServiceImpl inBookService)
    {
        bookService = inBookService;
    }

    @Operation(summary = "Obtenir tous les livres")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Liste des livres")})
    @GetMapping("")
    @JsonView(StructBook.Summary.class)
    public ResponseEntity<List<StructBook>> get()
    {
        return new ResponseEntity<>(bookService.getAllBooks(), HttpStatus.OK);
    }

    @Operation(summary = "Ajouter un nouveau livre", description = "NB: 'bookId' est ignoré et sera automatiquement généré", security = @SecurityRequirement(name = "ADMIN"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Livre ajouté"),
            @ApiResponse(responseCode = "400", description = "Requête invalide", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "403", description = "Droits insuffisants", content = { @Content(schema = @Schema() ) }),
    })
    @PostMapping("")
    @JsonView(StructBook.Summary.class)
    public ResponseEntity<StructBook> post(@RequestBody CreateBookDTO inBook)
    {
        StructBook saved;

        try
        {
            // On essaye de créer le livre...
            saved = bookService.createBook(inBook);
        }
        catch (IllegalArgumentException e)
        {
            // ...on renvoit une erreur si le livre n'est pas valide
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }

    @Operation(summary = "Obtenir un livre par son ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Livre trouvé"),
            @ApiResponse(responseCode = "404", description = "Livre non trouvé", content = { @Content(schema = @Schema() ) })})
    @GetMapping("/{id}")
    @JsonView(StructBook.Summary.class)
    public ResponseEntity<StructBook> getId(@PathVariable("id") Long id)
    {
        StructBook book = bookService.getBook(id);

        if (book == null)
        {
            // Le livre n'existe pas
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(book, HttpStatus.OK);
    }

    @Operation(summary = "Supprimer un livre", security = @SecurityRequirement(name = "ADMIN"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Livre supprimé", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "404", description = "Livre non trouvé", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "403", description = "Droits insuffisants", content = { @Content(schema = @Schema() ) }),
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteId(@PathVariable("id") Long id)
    {
        try
        {
            // On essaye de supprimer le livre...
            bookService.deleteBook(id);
        }
        catch (EntityNotFoundException e)
        {
            // ...on renvoit une erreur s'il n'existe pas
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Modifier un livre existant", description = "Seuls les paramètres non nuls seront utilisés.", security = @SecurityRequirement(name = "ADMIN"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Livre modifié"),
            @ApiResponse(responseCode = "400", description = "Requête invalide", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "304", description = "Le livre n'a pas été modifié", content = { @Content(schema = @Schema() ) }),
    })
    @PostMapping("/{id}")
    @JsonView(StructBook.Summary.class)
    public ResponseEntity<StructBook> postId(@PathVariable("id") Long id, @RequestBody CreateBookDTO inBook)
    {
        StructBook result;

        try
        {
            // On essaye de mettre ) jour le livre...
            result = bookService.updateBook(id, inBook);
        }
        catch (EntityNotFoundException e)
        {
            // ...erreur si le livre n'existe pas
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        catch (EntityExistsException e)
        {
            // ...les paramètres passés sont identiques à ceux du livre en base
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Rechercher des livres par titre et/ou auteur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Liste de livres correspondant à la recherche"),
            @ApiResponse(responseCode = "400", description = "Requête invalide", content = { @Content(schema = @Schema() ) })
    })
    @GetMapping("/search")
    @JsonView(StructBook.Summary.class)
    public ResponseEntity<List<StructBook>> getSearch(@RequestParam(name = "title", required = false) String title, @RequestParam(name = "author", required = false) String author)
    {
        List<StructBook> books;

        try
        {
            // On essaye de rechercher des livres...
            books = bookService.searchBooks(title, author);
        }
        catch (IllegalArgumentException e)
        {
            // ...erreur si aucun des paramètres n'est correct
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(books, HttpStatus.OK);
    }
}
