package com.lafaye.alexis.bibliotheque.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.lafaye.alexis.bibliotheque.dto.CreateUserDTO;
import com.lafaye.alexis.bibliotheque.dto.RegisterDTO;
import com.lafaye.alexis.bibliotheque.models.StructUser;
import com.lafaye.alexis.bibliotheque.services.UserServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserServiceImpl userService;

    public UserController(UserServiceImpl inUserService)
    {
        userService = inUserService;
    }

    @Operation(summary = "Ajouter un nouvel utilisateur", security = @SecurityRequirement(name = "ADMIN"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Utilisateur ajouté"),
            @ApiResponse(responseCode = "400", description = "Requête invalide", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "403", description = "Droits insuffisants", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "409", description = "Un utilisateur avec le même nom ou email existe déjà", content = { @Content(schema = @Schema() ) })
    })
    @PostMapping("")
    @JsonView(StructUser.Summary.class)
    public ResponseEntity<StructUser> post(@RequestBody CreateUserDTO inUser)
    {
        StructUser saved;

        // On essaye d'ajouter le nouvel user dans la base. Si les contraintes d'unicités sur le username
        // ou l'email ne sont pas respectées, on renvoit un code "Conflict"

        try
        {
            saved = userService.createUser(inUser);
        }
        catch (DataIntegrityViolationException e)
        {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        catch (IllegalArgumentException e)
        {
            // Un ou plusieurs paramètres invalides
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }

    @Operation(summary = "Obtenir la liste de tous les utilisateurs", security = @SecurityRequirement(name = "ADMIN"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Liste d'utilisateurs"),
            @ApiResponse(responseCode = "403", description = "Droits insuffisants", content = { @Content(schema = @Schema() ) })
    })
    @GetMapping("")
    @JsonView(StructUser.Summary.class)
    public ResponseEntity<List<StructUser>> get()
    {
        return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
    }

    @Operation(summary = "Obtenir un utilisateur par son ID", security = @SecurityRequirement(name = "ADMIN"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateur demandé"),
            @ApiResponse(responseCode = "400", description = "Requête invalide", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "403", description = "Droits insuffisants", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "404", description = "Utilisateur non trouvé", content = { @Content(schema = @Schema() ) })
    })
    @GetMapping("/{id}")
    @JsonView(StructUser.Summary.class)
    public ResponseEntity<StructUser> getId(@PathVariable("id") Long id)
    {
        StructUser user = userService.getUserById(id);

        if (user == null)
        {
            // Utilisateur introuvable
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @Operation(summary = "Supprimer un utilisateur", security = @SecurityRequirement(name = "ADMIN"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateur supprimé", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "400", description = "Requête invalide", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "403", description = "Droits insuffisants", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "404", description = "Utilisateur non trouvé", content = { @Content(schema = @Schema() ) })
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteId(@PathVariable("id") Long id)
    {
        try
        {
            // On essaye de supprimer l'utilisateur...
            userService.deleteUser(id);
        } catch (EntityNotFoundException e)
        {
            // ...erreur s'il n'existe pas
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Modifier un utilisateur existant", security = @SecurityRequirement(name = "ADMIN"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateur modifié"),
            @ApiResponse(responseCode = "304", description = "L'utilisateur n'a pas été modifié", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "400", description = "Requête invalide", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "403", description = "Droits insuffisants", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "404", description = "Utilisateur non trouvé", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "409", description = "Un utilisateur avec le même nom ou email existe déjà", content = { @Content(schema = @Schema() ) }),
    })
    @PostMapping("/{id}")
    @JsonView(StructUser.Summary.class)
    public ResponseEntity<StructUser> postId(@PathVariable("id") Long id, @RequestBody CreateUserDTO inUser)
    {
        StructUser result;

        try
        {
            // On esssaye de modifier l'utilisateur...
            result = userService.updateUser(id, inUser);
        }
        catch (EntityNotFoundException e)
        {
            // ...erreur s'il n'existe pas
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        catch (DataIntegrityViolationException e)
        {
            // ...erreur s'il entre en conflit avec un autre utilisateur
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        catch (EntityExistsException e)
        {
            // ...les champs passés sont identiques à ceux de l"utilisateur actuel
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Obtenir l'utilisateur actuel")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateur actuellement authentifié")
    })
    @GetMapping("/me")
    @JsonView(StructUser.Summary.class)
    public ResponseEntity<StructUser> getMe(@AuthenticationPrincipal UserDetails userDetails)
    {
        // Inutile de try...catch, on est garanti que l'utilisateur existe car cette route nécessite qu'il soit authentifié
        return new ResponseEntity<>(userService.getUserByUsername(userDetails.getUsername()), HttpStatus.OK);
    }

    @Operation(summary = "Modifier l'utilisateur actuel")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateur modifié"),
            @ApiResponse(responseCode = "304", description = "L'utilisateur n'a pas été modifié", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "400", description = "Requête invalide", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "409", description = "Un utilisateur avec le même nom ou email existe déjà", content = { @Content(schema = @Schema() ) }),
    })
    @PostMapping("/me")
    @JsonView(StructUser.Summary.class)
    public ResponseEntity<StructUser> postMe(@AuthenticationPrincipal UserDetails userDetails, @RequestBody RegisterDTO inUser)
    {
        // Inutile de try...catch, on est garantis que l'utilisateur existe car cette route nécessite qu'il soit authentifié
        StructUser user = userService.getUserByUsername(userDetails.getUsername());

        return postId(user.getUserId(), new CreateUserDTO(inUser.getUsername(), inUser.getEmail(), inUser.getPassword(), user.getRole()));
    }

    @Operation(summary = "Inscrire un nouvel utilisateur avec le rôle `USER`")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Utilisateur ajouté"),
            @ApiResponse(responseCode = "400", description = "Requête invalide", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "409", description = "Un utilisateur avec le même nom ou email existe déjà", content = { @Content(schema = @Schema() ) }),
    })
    @PostMapping("/register")
    @JsonView(StructUser.Summary.class)
    public ResponseEntity<StructUser> postMe(@RequestBody RegisterDTO inUser)
    {
        return post(new CreateUserDTO(inUser.getUsername(), inUser.getEmail(), inUser.getPassword(), "USER"));
    }
}
