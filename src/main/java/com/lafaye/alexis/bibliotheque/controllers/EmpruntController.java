package com.lafaye.alexis.bibliotheque.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.lafaye.alexis.bibliotheque.config.Config;
import com.lafaye.alexis.bibliotheque.dto.LateEmpruntsDTO;
import com.lafaye.alexis.bibliotheque.dto.UpdateEmpruntDTO;
import com.lafaye.alexis.bibliotheque.models.StructBook;
import com.lafaye.alexis.bibliotheque.models.StructEmprunt;
import com.lafaye.alexis.bibliotheque.models.StructUser;
import com.lafaye.alexis.bibliotheque.repositorises.StructEmpruntRepository;
import com.lafaye.alexis.bibliotheque.repositorises.StructUserRepository;
import com.lafaye.alexis.bibliotheque.services.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/emprunts")
public class EmpruntController {

    private final UserServiceImpl userService;

    private final EmpruntServiceImpl empruntService;

    public EmpruntController(UserServiceImpl inUserService, EmpruntServiceImpl inEmpruntService, BookServiceImpl inBookService)
    {
        userService = inUserService;
        empruntService = inEmpruntService;
    }

    @Operation(summary = "Obtenir tous les emprunts réalisés par l'utilisateur actuel",
            parameters = {
                @Parameter(name = "active",
                           description = "Si spécifié, permet d'obtenir seulement les emprunts en cours (c'est-à-dire qui n'ont pas encore été retournés) ou terminés (qui ont été retournés)",
                           content = { @Content(schema = @Schema( implementation = Boolean.class)) } )
            })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Liste des emprunts")
    })
    @GetMapping("")
    @JsonView(StructEmprunt.Summary.class)
    public ResponseEntity<List<StructEmprunt>> get(@AuthenticationPrincipal User userDetails, @RequestParam(name = "active", required = false) Boolean active)
    {
        StructUser user = userService.getUserByUsername(userDetails.getUsername());

        if (active != null)
        {
            return new ResponseEntity<>(empruntService.getUserEmprunts(user.getUserId(), active), HttpStatus.OK);
        }

        return new ResponseEntity<>(empruntService.getUserEmprunts(user.getUserId()), HttpStatus.OK);
    }

    @Operation(summary = "Emprunter un livre pour l'utilisateur actuel")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Livre emprunté"),
            @ApiResponse(responseCode = "400", description = "Requête invalide", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "403", description = "Le livre demandé n'est pas disponible, ou l'utilisateur a atteint le nombre maximale d'emprunts actifs simultanés", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "404", description = "Le livre spécifié n'existe pas"),
            @ApiResponse(responseCode = "409", description = "L'utilisateur a déjà un emprunt actif pour ce livre", content = { @Content(schema = @Schema() ) })
    })
    @PostMapping("")
    @JsonView(StructEmprunt.Summary.class)
    public ResponseEntity<StructEmprunt> post(@AuthenticationPrincipal UserDetails userDetails, @RequestParam("bookId") Long bookId)
    {
        StructUser user = userService.getUserByUsername(userDetails.getUsername());

        if (bookId == null)
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        StructEmprunt emprunt;

        try
        {
            // On essaye d'ajouter l'emprunt...
            emprunt = empruntService.addEmprunt(user.getUserId(), bookId);
        }
        catch (EntityNotFoundException e)
        {
            // ...erreur si l'utilisateur et/ou le livre n'existe pas
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        catch (EmpruntService.BookUnavailableException | EmpruntService.MaxActiveEmpruntsReachedException e)
        {
            // ...erreur si le livre n'est pas disponible ou si l'utilisateur ne peut pas emprunter
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        catch (EntityExistsException e)
        {
            // ...erreur si l'utilisateur a déjà un emprunt actif pour ce livre
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(emprunt, HttpStatus.CREATED);
    }

    @Operation(summary = "Retourner un emprunt pour l'utilisateur actuel")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "L'emprunt a bien été retourné"),
            @ApiResponse(responseCode = "304", description = "L'emprunt était déjà retourné", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "400", description = "Requête invalide", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "404", description = "Aucun emprunt n'a été trouvé pour le livre spécifié", content = { @Content(schema = @Schema() ) })
    })
    @DeleteMapping("")
    @JsonView(StructEmprunt.Summary.class)
    public ResponseEntity<StructEmprunt> delete(@AuthenticationPrincipal UserDetails userDetails, @RequestParam("bookId") Long bookId)
    {
        StructUser user = userService.getUserByUsername(userDetails.getUsername());

        if (bookId == null)
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        StructEmprunt emprunt;

        try
        {
            // On essaye de retourner le livre...
            emprunt = empruntService.returnEmprunt(user.getUserId(), bookId);
        }
        catch (EntityNotFoundException e)
        {
            // ...erreur si l'emprunt n'existe pas
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        catch (EntityExistsException e)
        {
            // ...l'emprunt avait déjà été retoruné
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }

        return new ResponseEntity<>(emprunt, HttpStatus.OK);
    }

    @Operation(summary = "Modifier un emprunt pour un utilisateur spécifié",
               security = @SecurityRequirement(name = "ADMIN"),
               description = """
                            Attention, si `returnDate` n'est pas spécifié ou nul, l'emprunt sera marqué comme 'actif'.
                            Pour modifier `limitReturnDate` uniquement, pensez à spécifier `returnDate` avec sa valeur actuelle.""" )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "L'emprunt a été modifié"),
            @ApiResponse(responseCode = "304", description = "L'emprunt n'a pas été modifié", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "400", description = "Requête invalide", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "403", description = "Droits insuffisants", content = { @Content(schema = @Schema() ) }),
            @ApiResponse(responseCode = "404", description = "Aucun emprunt n'a été trouvé pour l'utilisateur et le livre spécifiés", content = { @Content(schema = @Schema() ) })
    })
    @PostMapping("/edit")
    @JsonView(StructEmprunt.Summary.class)
    public ResponseEntity<StructEmprunt> postEdit(@RequestBody UpdateEmpruntDTO inEmprunt)
    {
        StructEmprunt emprunt;

        try
        {
            // On essaye de mettre à jour l'emprunt...
            emprunt = empruntService.updateEmprunt(inEmprunt);
        }
        catch (EntityNotFoundException e)
        {
            // ...erreur si l'emprunt n'existe pas
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        catch (EntityExistsException e)
        {
            // ...l'emprunt modifié est identique à l'emprunt actuel
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }

        return new ResponseEntity<>(emprunt, HttpStatus.OK);
    }

    @Operation(summary = "Obtenir le nombre d'emprunts retournés en retard pour l'utilisateur spécifié", security = @SecurityRequirement(name = "ADMIN"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Nombre d'emprunts retournés en retard"),
            @ApiResponse(responseCode = "404", description = "Utilisateur non trouvé", content = { @Content(schema = @Schema() ) })
    })
    @GetMapping("/late")
    public ResponseEntity<LateEmpruntsDTO> getLate(@RequestParam Long userId)
    {
        StructUser user = userService.getUserById(userId);

        if (user == null)
        {
            // Utilisateur introuvable
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new LateEmpruntsDTO(empruntService.getNumLateEmprunts(userId)), HttpStatus.OK);
    }

    @Operation(summary = "Obtenir tous les emprunts de tous les utilisateurs",
            parameters = {
                    @Parameter(name = "active",
                            description = "Si spécifié, permet d'obtenir seulement les emprunts en cours (c'est-à-dire qui n'ont pas encore été retournés) ou terminés (qui ont été retournés)",
                            content = { @Content(schema = @Schema( implementation = Boolean.class)) } )
            },
            security = @SecurityRequirement(name = "ADMIN"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Liste des emprunts")
    })
    @GetMapping("/all")
    @JsonView(StructEmprunt.Summary.class)
    public ResponseEntity<List<StructEmprunt>> getAll(@RequestParam(name = "active", required = false) Boolean active)
    {
        if (active != null)
        {
            return new ResponseEntity<>(empruntService.getEmprunts(active), HttpStatus.OK);
        }

        return new ResponseEntity<>(empruntService.getEmprunts(), HttpStatus.OK);
    }
}
