package com.lafaye.alexis.bibliotheque;

import com.lafaye.alexis.bibliotheque.repositorises.CustomRepositoryImpl;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(repositoryBaseClass = CustomRepositoryImpl.class)
@OpenAPIDefinition(info = @Info(title = "Bibliothèque Spring App API reference", description = """
										API permettant la gestion de livres et d'emprunts d'une bibliothèque.
										Toutes les routes listées ci-dessours nécessitent d'être authentifié.
										L'authentification se fait via HTTP basic auth.
										Les routes nécessitant le rôle `ADMIN` sont accompagnées d'un cadenas.""", version = "v1"))
public class BibliothequeApplication {

	public static void main(String[] args) {
		SpringApplication.run(BibliothequeApplication.class, args);
	}

}
