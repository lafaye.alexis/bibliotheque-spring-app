package com.lafaye.alexis.bibliotheque.config;

import com.lafaye.alexis.bibliotheque.services.BibliothequeUserDetailsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.server.csrf.CookieServerCsrfTokenRepository;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

        // Cette méthode permet de paramétrer les routes protégées par authentification, ainsi que l'authentification en elle-même
        // On y définit les routes accessibles sans authentification (permitAll), celles nécessitant d'être authentifié
        // (authenticated) et celles nécessitant les droits d'administrateur (hasAuthority("ADMIN"))

        http
            .authorizeHttpRequests(authorize -> authorize
                .requestMatchers("/login", "/users/register", "/h2-console/**", "/v2/api-docs/**").permitAll()
                .requestMatchers("/users/me").authenticated()
                .requestMatchers("/users/**").hasAuthority("ADMIN")
                .requestMatchers(HttpMethod.GET, "/books/**").authenticated()
                .requestMatchers("/books/**").hasAuthority("ADMIN")
                .requestMatchers("/emprunts/*").hasAuthority("ADMIN")
                .anyRequest().permitAll())
            .formLogin(withDefaults())
            .httpBasic(withDefaults())
            .csrf(csrf -> csrf.disable())
            .headers(headers -> headers.frameOptions(options -> options.disable()));

        return http.build();
    }

    @Bean
    public BibliothequeUserDetailsService userDetailsService() {
        // On met à disposition notre UserDetailsService custom qui gère la récupération des utilisateurs depuis la base de données

        return new BibliothequeUserDetailsService();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider(BibliothequeUserDetailsService userDetailsService) {
        // On définit un AuthenticationProvider qui utilise notre UserDetailsService et qui définit l'encruption des mots de passe

        DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
        auth.setUserDetailsService(userDetailsService);
        auth.setPasswordEncoder(new BCryptPasswordEncoder());
        return auth;
    }
}
