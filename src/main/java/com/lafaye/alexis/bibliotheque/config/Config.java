package com.lafaye.alexis.bibliotheque.config;

import java.time.Duration;

/**
 * Classe contenant les constantes globales utilisées par l'application
 */
public class Config {

    /* La durée maximum d'un emprunt avant qu'il soit considéré comme en retard */
    public static final Duration EMPRUNT_DURATION = Duration.ofDays(15);

    /* Le nombre maximum d'emprunts actifs simultanés que peut avoir un utilisateur */
    public static final int MAX_ACTIVE_EMPRUNTS_PER_USER = 3;

}
