# Application de gestion d'emprunts de livres pour une bibliothèque

Ce projet est une application Spring Boot de gestion d'emprunts de livres pour une bibliothèque. Il permet de créer, mettre à jour, supprimer et consulter des emprunts de livres effectués par les utilisateurs. L'application est développée en utilisant Maven comme outil de gestion de construction, de déploiement et de test.

## Prérequis

Avant de pouvoir construire, déployer et tester cette application, assurez-vous d'avoir les éléments suivants déjà installés:

- [Java Development Kit (JDK) version 19](https://www.oracle.com/java/technologies/javase/jdk19-archive-downloads.html)
- [Apache Maven](https://maven.apache.org/install.html)

## Construction de l'application

Pour construire l'application, suivez ces étapes:

   ```
   git clone git@gitlab.com:lafaye.alexis/bibliotheque-spring-app.git
   cd bibliotheque-spring-app
   mvn clean install
   ```

## Déploiement de l'application

Une fois l'application construite avec succès, vous pouvez la déployer de différentes manières, selon vos besoins. Voici deux méthodes couramment utilisées:

### Déploiement local

1. Exécutez la commande Maven suivante pour lancer l'application:
   ```
   mvn spring-boot:run
   ```
2. L'application sera maintenant accessible à `http://localhost:8080`.

## Tests de l'application

Pour exécuter les tests de l'application, suivez ces étapes:

1. Exécutez la commande Maven suivante pour exécuter les tests:
   ```
   mvn test
   ```
2. Les tests seront exécutés et les résultats s'afficheront dans la console.

## Documentation

Une documentation complète de l'API est disponible à l'addresse `http://localhost:8080/swagger-ui/index.html` une fois l'application lancée.

## Fonctionnement de l'application

Tous les endpoints de l'API nécessitent une authentification HTTP basic. Par défaut, un utilisateur avec le rôle administrateur est créé, dont les identifiants sont :
- username : `admin`
- password : `password`

### Gestion des emprunts

La fonctionnalité de gestion des emprunts permet aux utilisateurs de procéder à l'emprunt d'un livre dès lors qu'il est disponible. Tout utilisateur est restreint à un maximum de 3 emprunts simultanés. Dans le cas où un utilisateur atteint cette limite, il ne pourra emprunter un nouveau livre qu'après avoir retourné l'un de ses emprunts existants.

Un aspect important à noter est la notion de **retard**. Lorsqu'un utilisateur emprunte un livre, il dispose de 15 jours pour le retourner. Au-delà de ce délai, l'utilisateur sera sujet à un retard supplémentaire. Les administrateurs ont accès aux statistiques de retard des utilisateurs et peuvent prendre des mesures appropriées pour ceux qui accumulent des retards de manière répétée.

Pour obtenir des détails spécifiques sur l'utilisation de l'API, veuillez consulter la documentation disponible via le lien mentionné précédemment.

## Collection Postman

Pour faciliter les tests, vous avez à votre disposition à la racine de ce repos le fichier `Bibliotheque Spring App.postman_collection.json`. Vous pouvez importer cette collection dans Postman afin d'obtenir la liste des endpoints prêts à l'emploi.